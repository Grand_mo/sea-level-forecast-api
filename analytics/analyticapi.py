from flask import Flask, jsonify, request, g, abort
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite' + '?check_same_thread=False'
app.config['SQLALCHEMY_TRACK_MODIFICATION'] = False
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

db_ts = SQLAlchemy(app)     # times_series
db_f = SQLAlchemy(app)      # forecast


class Station(db_ts.Model):
    __tablename__ = 'stations'
    id = db_ts.Column(db_ts.Integer, primary_key=True)
    station_name = db_ts.Column(db_ts.String(20), index=True)
    data = db_ts.Column(db_ts.String(20), index=True)

    def set_data(self, data):
        self.data = data

    def post_proc_data(self):
        self.data = self.data

    def get_timeline(self):
        return self.data

@app.route('/analytics/station/<int:id>')
def get_data():
    station = Station.query.get(id)
    if not station:
        abort(400)
    return jsonify({'data': station.data})


@app.route('/analytics/station/parse/<int:id>', methods=['POST'])
def parse_data():
    station = Station.query.get(id)
    if not station:
        abort(400)

    data = request.json.get('data')
    if data is None:
        abort(400)

    station.set_data(data)
    station.post_proc_data()
    return None


class Forecast(db_f.Model):
    __tablename__ = 'forecast'
    id = db_f.Column(db_f.Integer, primary_key=True)
    station_name = db_f.Column(db_f.String(32), index=True)
    data = db_f.Column(db_f.String(20), index=True)
    settings = db_f.Column(db_f.String(20), index=True)

    def set_settings(self, settings):
        self.settings = settings

    def create_forecast(self):
        self.data = self.data

    def set_data(self, data):
        self.data = data


@app.route('/analytics/forecast/settings/<int:id>', methods=['POST'])
def install_settings():
    settings = request.json.get('settings')
    forecast = Forecast.query.get(id)
    if not forecast:
        abort(400)
    forecast.set_settings(settings)


@app.route('/analytics/forecast/make/<int:id>')
def make_forecast(id):
    station = Station.query.get(id)
    if not station:
        abort(400)
    forecast = Forecast.query.get(id)
    if not forecast:
        abort(400)
    data = station.get_timeline()
    forecast.set_data(data)
    forecast.create_forecast()


@app.route('/analytics/forecast/get/<int:id>', methods=['POST'])
def get_forecast():
    forecast = Forecast.query.get(id)
    if not forecast:
        abort(400)
    return jsonify({'data': forecast.data})


if __name__ == '__main__':
    if not os.path.exists('db_ts.sqlite'):
        db_ts.create_all()
    if not os.path.exists('db_f.sqlite'):
        db_f.create_all()
    app.run(host='0.0.0.0', port=5000, debug=True)
