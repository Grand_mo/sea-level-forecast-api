import os
import requests
from flask import Flask, jsonify, request, g, abort
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import Serializer, SignatureExpired, BadSignature

app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY') or 'my-secret-key'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite' + '?check_same_thread=False'
app.config['SQLALCHEMY_TRACK_MODIFICATION'] = False
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

db = SQLAlchemy(app)
auth = HTTPBasicAuth()


class Admin(db.Model):
    __tablename__ = 'admins'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    password_hash = db.column(db.String(64))
    changes = None

    def hash_password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def set_changes(self, changes):
        self.changes = changes

    def get_changes(self):
        return self.changes

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'])
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None
        admin = Admin.query.get(data['id'])
        return admin


@auth.verify_password
def verify_password(username_or_token, password):
    admin = Admin.verify_auth_token(username_or_token)
    if not admin:
        admin = admin.query.filter_by(username=username_or_token).first()
        if not admin or not admin.verify_password(password):
            return False
    g.admin = admin
    return True


@app.route('/authentication/admin/<int:id>')
def get_adm(id):
    admin = Admin.query.get(id)
    if not admin:
        abort(400)
    return jsonify({'username': admin.username})


@app.route('/authentication/token')
@auth.login_required
def get_auth_token():
    token = g.admin.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@app.route('/authentication/admin/changes', methods=['POST'])
@auth.login_required()
def set_settings():
    settings = request.json.get('settings')
    if settings is None:
        abort(400)
    g.admin.set_changes(settings)
    return jsonify({'username': g.admin.username, 'settings': settings})


@app.route('/authentication/admin/changes/confirm', methods=['POST'])
@auth.login_required()
def confirm_settings(admin_id, id):
    admin = Admin.query.get(admin_id)
    if not admin:
        abort(400)
    settings = admin.get_changes()
    paras = {'settings': settings}
    url = 'http:///0.0.0.0:5000/analytics/forecast/settings/'
    url += str(id)
    requests.post(url=url, data=paras)
    return None


if __name__ == '__main__':
    if not os.path.exists('db.sqlite'):
        db.create_all()
    app.run(host='0.0.0.0', port=5002, debug=True)
