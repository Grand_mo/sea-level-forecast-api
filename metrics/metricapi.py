from flask import Flask
import requests


app = Flask(__name__)


@app.route('/metrics/show/', methods=['POST'])
def show_forecast(id):
    url = 'http:///0.0.0.0:5000/analytics/forecast/get/'
    url += str(id)
    data = requests.post(url=url)
    print(data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
